<?php
class StudentInfo
{
    public function doSomething()
    {
        echo "Hello world";
    }
    public function __callStatic($name,$arguments)
    {
        echo $name."<br>";
        print_r($arguments);          //here arguments are array so that we use print_r
    }
}

StudentInfo::messageBITM(65,45,"hi there");//callStatic method is called when wrong method is called by class
echo "<br>";

$myObj=new StudentInfo("I am nothing");
