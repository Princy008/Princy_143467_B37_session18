<?php
class StudentInfo
{    //accessable for private and public variable
   // public  $myVar="Princy";

    public function __isset($name)  //when doesnt match variable then jetengine takes the new var
                                   // prints true/false
    {
        echo $name;
    }


}
$obj=new StudentInfo();

//echo $obj->var;       //this line is prints fatal error,for handling this error using isset


var_dump(isset($obj->myVar)) ;//isset use for handling  inaccessable variable so that the prgm doesnt crash.find myVar.
 //echo ($result);     //prints 1 bcz


$result=isset($obj->Var) ;//isset use for handling  inaccessable variable so that the prgm doesnt crash.
echo $result;              //prints Var cz doesnt match myVar