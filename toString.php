<?php
class TestClass
{
public $foo;

public function __construct($foo)
{
$this->foo = $foo;
}

public function __toString()
{
return $this->foo;    //local foo is set into global foo,prints Hello
 //return "world";//prints World bcz its global variable
    //  return strval(1000);
}
}

$obj = new TestClass('Hello');//The __toString() method allows a class to decide how it will react when it is treated like a strin
echo $obj;               //what echo $obj; will print. This method must return a string,
                        // as otherwise a fatal E_RECOVERABLE_ERROR level error is emitted.
?>