<?php
class StudentInfo
{
 public function doSomething()
 {
     echo "Hello world";
 }
    public function __call($name,$arguments)//this method is called when object can't match any method
                                             //here doSomething metrhod exists but doSomethingUUU is not exists
    {                                        //jetengine is called this method here
       echo $name;
    }
}
$myObj=new StudentInfo();

$myObj->doSomethingUUU();     //not matching the method doSomething