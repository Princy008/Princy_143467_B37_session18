<?php
class ImAClass
{
    public function __construct($value)
    {
        echo $value."<br>";
    }
    public function __destruct()
    {
        echo "good bye"."<br>";
    }

}
$obj=new ImAClass("I am inside __construct");
unset($obj);
echo "Hello world";