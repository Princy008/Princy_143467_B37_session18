<?php
class StudentInfo
{
    public  $myVar1;
    public  $myVar2;
    public  $myVar3;
    public  $myVar4;
    public function __sleep()
    {
        echo "I am inside sleep method";
        return array('myVar1','myVar2');//here,if  i want serialized myVar1 and myVar2 ,i write thats two property
    }


}
$obj=new StudentInfo();
$test=serialize($obj);      //serializ() method is called
echo($test);


//output:
//I am inside sleep methodO:11:"StudentInfo":2:{s:6:"myVar1";N;s:6:"myVar2";N;}