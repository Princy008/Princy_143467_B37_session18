<?php
class StudentInfo
{
    public  $myVar1;
    public  $myVar2;
    public  $myVar3;
    public  $myVar4;
    public function __wakeup()
    {
        echo "I am inside wake method";
        return array('myVar1','myVar2');
    }


}
$obj=new StudentInfo();
$test=serialize($obj);      //serializ() method is called
echo($test);
$arr=unserialize($test);     //serializ() method is called

//output:

//O:11:"StudentInfo":4:{s:6:"myVar1";N;s:6:"myVar2";N;s:6:"myVar3";N;s:6:"myVar4";N;}I am inside wake method